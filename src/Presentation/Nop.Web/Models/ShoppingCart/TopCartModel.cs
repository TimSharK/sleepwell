using System.Collections.Generic;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.ShoppingCart
{
    public partial class TopCartModel : BaseNopModel
    {
        public TopCartModel()
        {
            ShoppingCartItems = new List<TopShoppingCartItemModel>();
        }

        public List<TopShoppingCartItemModel> ShoppingCartItems { get; set; }
    }
}