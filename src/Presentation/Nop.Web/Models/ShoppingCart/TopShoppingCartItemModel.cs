using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Media;

namespace Nop.Web.Models.ShoppingCart
{
    public partial class TopShoppingCartItemModel : BaseNopModel
    {
        public TopShoppingCartItemModel()
        {
            Picture = new PictureModel();
        }
        public PictureModel Picture { get; set; }

        public int Quantity { get; set; }

        public string ProductName { get; set; }

        public string ProductSeName { get; set; }

        public string Price { get; set; }
    }
}