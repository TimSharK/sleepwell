(function($) {
"use strict";

/* ==============================================
TABBED HOVER -->
=============================================== */
  
  $('.nav-pills > li ').hover( function(){
    if($(this).hasClass('hoverblock'))
      return;
      else
      $(this).find('a').tab('show');
  });

  $('.nav-tabs > li').find('a').click( function(){
    $(this).parent()
      .siblings().addClass('hoverblock');
  });

  $(document).ready(function(){
      $('.topbarhover').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
      }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
      });  
  });

  $(".ddd").on("click", function () {
      var $button = $(this);
      var $input = $button.closest('.sp-quantity').find("input.quntity-input");
      
      $input.val(function(i, value) {
          return +value + (1 * +$button.data('multi'));
      });
  });

/* ==============================================
MENU HOVER -->
=============================================== */

    jQuery('.hovermenu .dropdown').hover(function() {
      jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    }, function() {
      jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    });

/* ==============================================
MENU CLICKABLE for HORIZONTAL -->
=============================================== */

  $('.clickablemenu .dropdown').click('show.bs.dropdown', function(e){
    var $dropdown = $(this).find('.dropdown-menu');
      var orig_margin_top = parseInt($dropdown.css('margin-top'));
      $dropdown.css({'margin-top': (orig_margin_top + 65) + 'px', opacity: 0}).animate({'margin-top': orig_margin_top + 'px', opacity: 1}, 420, function(){
        $(this).css({'margin-top':''});
    });
  });

/* ==============================================
MENU CLICKABLE for VERTICAL -->
=============================================== */

  $('.verticalmenu .dropdown').click('show.bs.dropdown', function(e){
    var $dropdown = $(this).find('.dropdown-menu');
      var orig_margin_top = parseInt("1", 10);
      $dropdown.css({'margin-left': (orig_margin_top + 65) + 'px', opacity: 0}).animate({'margin-left': orig_margin_top + 'px', opacity: 1}, 420, function(){
         $(this).css({'margin-left':''});
    });
  });

/* ==============================================
TOOLTIP -->
=============================================== */

  $('body').tooltip({
    selector: "[data-tooltip=tooltip]",
    container: "body"
  });

/* ==============================================
LOADER -->
=============================================== */

    $(window).load(function() {
        $('#loader').delay(300).fadeOut('slow');
        $('#loader-container').delay(200).fadeOut('slow');
        $('body').delay(300).css({'overflow':'visible'});
    })

/* ==============================================
ANIMATION -->
=============================================== */

    new WOW({
    boxClass:     'wow',      // default
    animateClass: 'animated', // default
    offset:       0,          // default
    mobile:       true,       // default
    live:         true        // default
    }).init();

/* ==============================================
AjaxCart settings -->
=============================================== */

    AjaxCart.topcartselector = ".top-cart__count-wrapper";
    AjaxCart.flyoutcartselector = ".topbar-cart > .btn-group";

})(jQuery);